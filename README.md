# kigen-starter

### Description

Starters provide a set of convenient dependency descriptors that we can include in our application. 
We get a one-stop-shop for all the stack and related technology that we need without having to find through 
sample code and copy paste loads of dependency descriptors. For example, if we want to get started using Kigen 
and JPA for database access just include the kigen-starter-data-jpa dependency in our project, and we are good 
to go.

Starters are provided by Kigen framework under the com.everis.kigenframework.starter group

### Using a starter

In order to use starters into a Kigen framework project, we have to include the starter dependency in our
build.gradle file. With this addition, we will have all the dependencies needed to start a new project



